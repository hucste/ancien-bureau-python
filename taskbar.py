#!/usr/bin/python3
# -*- coding: utf8 -*-

# Copyright © 2018 Benoît Boudaud <https://miamondo.org>
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>

#==== IMPORTATION DES MODULES ================================================

import tkinter as tk # Importation du module tkinter
import os
from PIL import Image, ImageTk
import sys
import menu

#==== CLASSES ET MÉTHODES ====================================================

class Taskbar(object):
    "Instanciation de la barre de tâches"

#-----------------------------------------------------------------------------

    def __init__(self):
        "Constructeur"

#-----------------------------------------------------------------------------

    def taskbar(self):
        "Instanciation de la barre de tâches"

        self.root = tk.Tk()
        self.root.overrideredirect(1)
        self.label=tk.Label(self.root, bg='black', height=30, width=self.root.winfo_screenwidth())
        self.label.grid()
        self.label.grid_propagate(0)
        self.dir_name = os.path.dirname(sys.argv[0])

        self.root.geometry(str(self.root.winfo_screenwidth()) +'x30+0+' +\
        str(self.root.winfo_screenheight() - 30))

        self.commando = menu.Menu()
        main_icon_size = 30, 30
        main_icon = Image.open(self.dir_name + '/icones/computer.png')
        main_icon.thumbnail(main_icon_size)
        icon_30 = ImageTk.PhotoImage(main_icon, master = self.root)
        main_icon = [icon_30]

        self.menu_button = tk.Button(self.label, width = 30, height = 30, image = main_icon, command=self.commando.menu)
        self.menu_button.grid(row=0, column=0, sticky='w')

        self.root.mainloop()

# == Main programm ===========================================================

if __name__ == "__main__":

    taskbar = Taskbar() # Création de l'objet "taskbar"
    taskbar.taskbar() # Appel de la méthode taskbar() sur l'objet "taskbar"

