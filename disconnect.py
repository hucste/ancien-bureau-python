#!/usr/bin/env python3
# -*- coding: utf8 -*-

# Copyright © 2019 Benoît Boudaud <https://miamondo.org>
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>

from tkinter import*
import os
from PIL import Image, ImageTk
import sys
import locale

#=============================================================================

class Exit(object):
    "Instanciation de la fenêtre et des boutons"

#-----------------------------------------------------------------------------

    def __init__(self):
        "constructeur"
        self._locale = locale.setlocale(locale.LC_ALL, "")[0:2]
        self.language = [
                        ["Abmeldungsfenster",
                         " Bitte auswählen ",
                         "Abbrechen",
                         "Abmelden",
                         "Bereitschaft",
                         "Neustarten",
                         "Herunterfahren"],
                        ["Logout window",
                         " Please choose an option ",
                         "Cancel",
                         "Logout",
                         "Standby",
                         "Reboot",
                         "Shutdown"],
                        ["Fenêtre de déconnexion",
                         " Faites votre choix ",
                         "Annuler",
                         "Quitter la session",
                         "Mettre en veille",
                         "Redémarrer",
                         "Éteindre"]
                        ]

        # Choix de la langue
        if self._locale == "de":
            self.locale_lang = self.language[0]
        elif self._locale == "en":
            self.locale_lang = self.language[1]
        elif self._locale == "fr":
            self.locale_lang = self.language[2]
        else:
            self.locale_lang = self.language[1]

#-----------------------------------------------------------------------------

    def exit(self):
        "Instanciation de la fenêtre et des boutons"

        self.root = Tk()
        self.root.overrideredirect(1)

        x = (self.root.winfo_screenwidth() - self.root.winfo_reqwidth()) / 5
        y = (self.root.winfo_screenheight() - self.root.winfo_reqheight()) / 2
        self.root.geometry("+%d+%d" % (x, y))

        # Traitement des images
        self._64_image_size = 64, 64
        self.images = ["/icones/cross.png",
                       "/icones/logout.png",
                       "/icones/suspend.png",
                       "/icones/reboot.png",
                       "/icones/shutdown.png"]

        for i, element in enumerate (self.images):
            self.image = Image.open(os.path.dirname(sys.argv[0])+ self.images[i])
            self.image.thumbnail(self._64_image_size)
            self.img = ImageTk.PhotoImage(self.image, master = self.root)
            self.images[i:i+1] = [self.img]

        # Création des widgets
        self.main_frame = Frame(self.root, bg="white")
        self.main_frame.grid()
        self.label_frame = LabelFrame(self.main_frame, bg="white", fg="black", bd=4,\
        font=("Times 18 bold"),\
        text=self.locale_lang[1], labelanchor="n", relief="ridge")
        self.label_frame.grid(padx=10, pady=10)

        self.sleep = Actions(self.root) # Création de l'objet self.sleep spécifique au bouton de mise en veille
        self.commands = [lambda:self.root.destroy(),
                         lambda:os.system("openbox --exit"),
                         lambda:self.sleep.suspend(self.root),
                         lambda:os.system("systemctl reboot"),
                         lambda:os.system("systemctl poweroff")]

        self.buttons = []
        for i, element in enumerate(self.commands):
            self.button = Button(self.label_frame, bg="white",bd=0,\
            highlightbackground="black", highlightthickness=2,\
            image=self.images[i], relief="ridge", height=70,\
            width=70, command=self.commands[i])
            self.buttons.append(self.button)
            self.button.grid(row=0, column=i, padx=10, pady=10, sticky="nsew")\
            if i <= 2 else self.button.grid(row=1, column=i-3, padx=10, pady=10, sticky="nsew")
            self.button.grid(columnspan=2) if i == 4 else self.button.grid(columnspan=1)

        # Liaison des événements aux boutons.
        self.list_enter_leave = []
        for i, element in enumerate(self.images):
            self.enter_leave = Actions(self.images[i], self.locale_lang[i+2])
            self.list_enter_leave.append(self.enter_leave)
        for i, element in enumerate(self.buttons):
            self.buttons[i].bind("<Enter>", self.list_enter_leave[i].texte)
            self.buttons[i].bind("<Leave>", self.list_enter_leave[i].back_to_image)

        self.root.mainloop() # Déclenchement du réceptionnaire d'événements

#=============================================================================

class Actions(object):
    "Commandes et événements"

#-----------------------------------------------------------------------------

    def __init__(self, image=None, button_name=None):
        "constructeur"

        self.image = image
        self.button_name = button_name

#-----------------------------------------------------------------------------

    def suspend(self, root):
        """Méthode nécessaire pour la mise en veille de l'ordinateur
           (impossible d'utiliser une expression lambda)"""
        self.root=root
        os.system("systemctl suspend")
        self.root.destroy()

#-----------------------------------------------------------------------------

    def texte(self, event):
        """Remplace l'icône par le texte décrivant la fonction du bouton
           lorsque la souris pénètre dans le widget"""
        event.widget.config(image="", text=self.button_name, activebackground="black",\
                    activeforeground="white", font=("Times 14 bold"), height=2, width=15)

    def back_to_image(self, event):
        """Remplace le texte par l'icône correspondante
           lorsque la souris quitte le widget"""
        event.widget.config(image=self.image, height=70, width=70)

#=============================================================================

# PROGRAMME PRINCIPAL:

"""Instruction qui sert à déterminer si le module est lancé
   en tant que programme autonome (auquel cas les instructions
   qui suivent doivent être exécutées), ou au contraire utilisé
   comme une bibliothèque de classe importée ailleurs.
   Dans ce cas, cette partie de code est sans effet."""

if __name__ == "__main__":

    exit = Exit()
    exit.exit()

