#!/usr/bin/python3
# -*- coding: utf8 -*-

# Copyright © 2019 Benoît Boudaud <https://miamondo.org>
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>

#==== IMPORTATION DES MODULES ================================================

import tkinter as tk # Importation du module tkinter
import tkinter.messagebox as tkmess
import tkinter.filedialog as tkfile
import os
from PIL import Image, ImageTk
import sys
import disconnect

#==Classes et méthodes========================================================

class Menu(object):
    "Instanciation des fenêtres et traitement des images"

#-----------------------------------------------------------------------------

    def __init__(self):
        "Constructeur"

#-----------------------------------------------------------------------------

    def menu(self):
        "Instanciation des fenêtres et traitement des images"

        # Chemin du répertoire parent
        self.dir_name = os.path.dirname(sys.argv[0])
        self.root = tk.Tk()
        self.root.attributes('-type', 'splash')
        self.label = tk.Label(self.root, bg='white', bd=2, relief='ridge')
        self.label.grid()

        # Placement de la fenêtre-mère en bas à gauche :
        self.root.geometry("+%d-%d" % (0, 30))

        self.top_button = tk.Tk()
        self.top_button.overrideredirect(1)
        self.top_button.geometry("+%d-%d" % (0, -5))

        main_icon_size = 30, 30
        main_icon = Image.open(self.dir_name + '/icones/computer.png')
        main_icon.thumbnail(main_icon_size)
        icon_30 = ImageTk.PhotoImage(main_icon, master = self.top_button)
        main_icon = [icon_30]

        self.menu_button = tk.Button(self.top_button, bg='white', bd=0,\
        image=main_icon, command=self.destroy_app)
        self.menu_button.grid()


        # Création des catégories :
        internet, multimedia, bureautique, development, graphisme, accessoires,\
        network, system = (None,) * 8

        # Création de l'objet "exit" (déconnexion, extinction, redémarrage etc...)
        self.shutdown = disconnect.Exit()

        # liste contenant l'image illustrant chaque catégorie
        # ainsi que la commande et le nom qui lui sont associés :

        icons = [['/terminal.png', ['xterm', 'Terminal']],
                 ['/thunar.png', ['thunar', 'Fichiers']],
                 ['/internet.png', [internet, 'Internet']],
                 ['/applications.png', ['thunar /usr/share/applications', 'Applications']],
                 ['/multimedia.png', [multimedia, 'Multimédia']],
                 ['/bureautique.png', [bureautique, 'Bureautique']],
                 ['/development.png', [development, 'Développement']],
                 ['/graphisme.png', [graphisme, 'Graphisme']],
                 ['/accessoires.png', [accessoires, 'Accessoires']],
                 ['/network.png', [network, 'Réseau']],
                 ['/system.png', [system, 'Système']],
                 ['/shutdown.png', [self.shutdown.exit, 'Déconnexion']]]

        self.j, self.k = 0, 0 # Variables de positionnement des icônes
        icon_size = 48, 48 # Tuple définissant la taille des icônes

        # Traitement des images :

        for element in icons:
            icon = Image.open(self.dir_name + '/icones' + element[0])
            icon.thumbnail(icon_size)
            icon_48 = ImageTk.PhotoImage(icon, master = self.root)
            element[0] = [icon_48]
            if self.j % 3 == 0 :
                self.k += 1
                self.j = 0
            self.j+=1
            categorie = Categories(root=self.root, label=self.label,\
            element=element[0], app_command=element[1], j=self.j, k=self.k,\
            dir_name=self.dir_name, top_button=self.top_button)

            #Appel de la méthode "catégorie" sur l'objet du même nom
            categorie.categorie()

        self.root.mainloop()

#-----------------------------------------------------------------------------

    def destroy_app(self) :
        "détruit le menu principal"

        self.root.destroy()
        self.top_button.destroy()

#=============================================================================

class Categories(object) :
    "Instancie les boutons des différentes catégories"

#-----------------------------------------------------------------------------

    def __init__(self, root=None, label=None, element=None, app_command=None,\
    j=None, k=None, dir_name=None, top_button=None):
        "Constructeur"

        self.root = root
        self.label = label
        self.element = element
        self.app_command = app_command
        self.j = j
        self.k = k
        self.dir_name = dir_name
        self.top_button=top_button

#-----------------------------------------------------------------------------

    def categorie(self):
        "Instancie les boutons différentes catégories"

        self.labelframe = tk.LabelFrame(self.label, labelanchor='n',\
        text=self.app_command[1], font="Bold 8", bg='white', fg='black',\
        bd=0, padx=5)
        self.labelframe.grid(row=self.k, column=self.j, padx=5, pady=5)

        self.app_button = tk.Button(self.labelframe, image=self.element,\
        bg='white')
        self.app_button.grid(row=self.k, column=self.j, padx=15, pady=5)

        if type(self.app_command[0]) == str :
            self.app_button.configure(command=self.commande)
        elif self.app_command[1] == 'Déconnexion':
            self.app_button.configure(command=self.shut_down)
        else :
            with open (self.dir_name + '/' + self.app_command[1], 'r') as file_:
                self.application_list = file_.readlines()
            self.app_command[0] = Icones(root=self.root, label=self.label,\
            element=self.element, app_command=self.app_command, j=self.j,\
            k=self.k, application_list = self.application_list,\
            dir_name=self.dir_name, top_button=self.top_button)
            self.app_button.configure(command=self.app_command[0].application)

#-----------------------------------------------------------------------------

    def commande(self):
        "Commande"

        os.system(self.app_command[0] + '&')
        self.root.destroy()
        self.top_button.destroy()

#-----------------------------------------------------------------------------

    def shut_down(self):
        """Ouverture de la fenêtre de déconnexion
           et destruction du menu principal"""

        self.root.destroy()
        self.app_command[0]()

#=============================================================================

class Icones(object) :
    "Création des objets des différentes applications"

#-----------------------------------------------------------------------------

    def __init__(self, root=None, label=None, element=None, app_command=None,\
    j=None, k=None, application_list=None, dir_name=None, top_button=None) :
        "Constructeur"

        self.root = root
        self.label = label
        self.element = element
        self.app_command = app_command
        self.j = j
        self.k = k
        self.application_list = application_list
        self.dir_name = dir_name
        self.top_button=top_button

#-----------------------------------------------------------------------------

    def application(self):
        "Création des objets des différentes applications"

        for child in self.label.winfo_children():
            if child.winfo_class()=='Labelframe' and child['bd'] > 0:
                child.grid_remove()
        self.labelframe = tk.LabelFrame(self.label, labelanchor='n',\
        bg='#FFFFCC', fg='white', bd=4, padx=15)
        self.labelframe.grid(row = 1, column = self.j+3, rowspan=15,\
        padx=5, pady=5, sticky='n')

        list_item1, list_item2, list_item3 = [], [], []
        for i, item in enumerate(self.application_list):
            item = item.strip()
            if i % 3 == 0 :
                item1 = item
                list_item1.append(item1) #commande

            if item.startswith('/') and item.endswith(".png"):
                item2 = item
                list_item2.append(item2) #image

            elif not i % 3 ==0 and not item.startswith('/'):
                item3 = item
                list_item3.append(item3) # nom de l'application

        for i, element in enumerate(list_item1):
            self.k+=1
            self.appli = Applications(root=self.root, label=self.label,\
            element=self.element, app_command=self.app_command, j=self.j, k=self.k,\
            application_list=self.application_list, labelframe=self.labelframe,\
            item1=list_item1[i], item2=list_item2[i], item3=list_item3[i],\
            dir_name=self.dir_name, top_button=self.top_button)

            self.appli.application()

#=============================================================================

class Applications(object):
    "Instancie les boutons des différentes applications"

#-----------------------------------------------------------------------------

    def __init__(self, root=None, label=None, element=None, app_command=None,\
    j=None, k=None, application_list=None, labelframe=None, item1=None, item2=None,\
    item3=None, dir_name=None, top_button=None):
        "Constructeur"

        self.root = root
        self.label = label
        self.element = element
        self.app_command = app_command
        self.j = j
        self.k = k
        self.application_list = application_list
        self.labelframe = labelframe
        self.item1 = item1
        self.item2 = item2
        self.item3 = item3
        self.dir_name = dir_name
        self.top_button = top_button

#-----------------------------------------------------------------------------

    def application(self):
        "Instancie les boutons des différentes applications"

        icon_size=32, 32
        icon = Image.open(self.dir_name + '/icones' + self.item2)
        icon.thumbnail(icon_size)
        icon_32 = ImageTk.PhotoImage(icon, master = self.root)
        self.item2 = [icon_32]

        self.lab_fr = tk.LabelFrame(self.labelframe, bg='#FFFFCC', bd=0, labelanchor='n',\
        text=self.item3)
        self.lab_fr.grid(row = self.k, column = self.j, padx=5, pady=5)

        self.commande=Commande(root=self.root, label=self.label,\
        item1=self.item1, item2=self.item2, item3=self.item3,\
        app_command=self.app_command, dir_name=self.dir_name, labelframe=self.labelframe, top_button=self.top_button)

        self.app_button = tk.Button(self.lab_fr, image=self.item2,\
        bg='white', command=self.commande.commande)
        self.app_button.pack()
        self.root.geometry("+%d-%d" % (0, 30))

#=============================================================================

class Commande(object):
    "Lancement des commandes"

#-----------------------------------------------------------------------------

    def __init__(self, root=None, label=None, item1=None, item2=None, item3=None,\
    app_command=None, dir_name=None, labelframe=None,  top_button=None):
        "constructeur"

        self.root = root
        self.label = label
        self.item1 = item1
        self.item2 = item2
        self.item3 = item3
        self.app_command = app_command
        self.dir_name = dir_name
        self.labelframe = labelframe
        self.top_button = top_button

#-----------------------------------------------------------------------------

    def commande(self):
        "Lancement des commandes"

        if self.item1 == 'ajouter' :
            for child in self.labelframe.winfo_children():
                child.grid_remove()
            self.frame = tk.Frame(self.labelframe, bg='#FFFFCC')
            self.frame.grid(row=2, column=4, rowspan=10, sticky='nsew')
            self.label_add_app = tk.Label(self.frame, bg='#FFFFCC', font="Bold 14", justify='left',\
            text='Le répertoire d\'applications(/usr/share/applications)\n va s\'ouvrir. Veuillez sélectionner l\'application\nque vous souhaitez rajouter au menu principal.')
            self.label_add_app.grid(row=0, column=1, padx=10, pady=10)
            self.add_ok = tk.Button(self.frame, bg='white', font='Bold 14', text='OK', command=self.show_files)
            self.add_ok.grid(row=1, column=1, padx=10, pady=5)

        elif self.item1 == 'retirer' :
            self.frame = tk.Frame(self.labelframe, bg='#FFFFCC')
            self.frame.grid(row=2, column=4, rowspan=10, sticky='nsew')
            self.label_suppress_app = tk.Label(self.frame, bg='#FFFFCC', font="Bold 14",\
            text='Entrez le nom de l\'application\nque vous souhaitez retirer\ndu menu principal.')
            self.label_suppress_app.grid(row=0, column=1, padx=10, pady=10, sticky='nsew')
            self.suppress_app = tk.Entry(self.frame, font="Bold 14", width=30)
            self.suppress_app.focus_force()
            self.suppress_app.grid(row=1, column=1,padx=10, pady=5, sticky='nsew')
            self.suppress_app.bind('<KeyPress-Return>', self.erase)

        else :
            os.system(self.item1 + ' &')
            self.root.destroy()
            self.top_button.destroy()

#-----------------------------------------------------------------------------

    def show_files(self):
        "Sélection de l'application à rajouter"

        self.add_ok.destroy()
        self.label_add_app.configure(text='Faites votre choix', font="Bold 14")
        self.listbox_add_app = tk.Listbox(self.frame, selectbackground='#FFFFCC', selectborderwidth=2)
        self.listbox_add_app.grid(row=2, column=1, pady=5, padx=5)
        self.app_liste = os.listdir('/usr/share/applications')
        self.app_liste.sort(key=str.lower)
        for i, app in enumerate(self.app_liste):
            self.listbox_add_app.insert('end', ' ' + app.replace('.desktop', ''))
        self.listbox_add_app.bind('<Double-Button-1>', self.select_app)

#-----------------------------------------------------------------------------

    def select_app(self, event):
        """Sélectionne l'application dans la liste déroulante"""

        self.commandos = []
        self.selected_app = '/usr/share/applications/' +\
        self.listbox_add_app.get('active').strip() + '.desktop'

        with open(self.selected_app, 'r') as file_ :
            self.desktop_file = file_.readlines()
            for line in self.desktop_file :
                if line.startswith('Exec='):
                    if '%U' in line :
                        line = line.replace('%U','')
                    elif '%u' in line :
                        line = line.replace('%u','')
                    elif '%F' in line :
                        line = line.replace('%F','')
                    elif '%f' in line :
                        line = line.replace('%f','')
                    elif line.endswith('-pkexec\n'):
                        line = 'xterm -e \"sudo ' + line.replace('-pkexec\n', '') + '\"'
                    self.commandos.append(line.strip().replace('Exec=', ''))
                    break

            for line2 in self.desktop_file :
                if line2.startswith('Name[fr'):
                    line2 = line2.split('=')
                    self.commandos.append(line2[1].strip())
                    break
                elif line2.startswith('Name='):
                    line2 = line2.split('=')
                    self.commandos.append(line2[1].strip())
                    break

        self.add_ok_no = tk.LabelFrame(self.frame, bg='#FFFFCC', bd=0)
        self.add_ok_no.grid(row=1, column=1, padx=10, pady=5)
        self.add_ok = tk.Button(self.add_ok_no, bg='white', font='Bold 14', text='Oui', command=self.select_icon)
        self.add_ok.grid(padx=10, pady=5)
        self.add_no = tk.Button(self.add_ok_no, bg='white', font='Bold 14', text='Non', command=self.insert_default_icon)
        self.add_no.grid(row=0, column=1)
        self.listbox_add_app.grid_remove()
        self.label_add_app.configure\
        (text='Souhaitez-vous remplacer l\'icône\npar défaut par une autre icône?', font='Bold 14')

#-----------------------------------------------------------------------------

    def select_icon(self):
        """"sélectionne l'application dans le répertoire /usr/share/applications"""

        self.add_ok_no.grid_remove()
        self.label_add_app.configure(text='Faites votre choix')
        self.listbox_add_app = tk.Listbox(self.frame, selectbackground='#FFFFCC', selectborderwidth=2)
        self.listbox_add_app.grid(row=2, column=1, pady=5, padx=5)
        self.app_liste = os.listdir(self.dir_name + '/icones')
        self.app_liste.sort(key=str.lower)
        for i, app in enumerate(self.app_liste):
            self.listbox_add_app.insert('end', os.path.basename(app))
        self.listbox_add_app.bind('<Double-Button-1>', self.insert_icon)

#-----------------------------------------------------------------------------

    def insert_icon(self, event) :
        """Intègre l'icône sélectionnée"""

        self.commandos.insert(1, '/' + self.listbox_add_app.get('active').strip())
        with open(self.dir_name + '/' + self.app_command[1], 'r') as file_ :
            self.application_list = file_.readlines()
            for commando in self.commandos:
                self.application_list.insert(-6, commando + '\n')
        with open(self.dir_name + '/' + self.app_command[1], 'w') as file_2 :
            for item in self.application_list:
                file_2.write(item)
        for child in self.frame.winfo_children():
            if child.winfo_class() == 'Button' or child.winfo_class() == 'Listbox':
                child.grid_remove()
        self.label_add_app.configure(text='L\'application a été rajoutée')
        self.root.after(2000, self.root.destroy)
        self.top_button.after(2000, self.top_button.destroy)

#-----------------------------------------------------------------------------

    def insert_default_icon(self):
        """Insertion de l'icône par défaut"""

        self.commandos.insert(1, '/default-app.png')
        with open(self.dir_name + '/' + self.app_command[1], 'r') as file_ :
            self.application_list = file_.readlines()
            for commando in self.commandos:
                self.application_list.insert(-6, commando + '\n')
        with open(self.dir_name + '/' + self.app_command[1], 'w') as file_2 :
            for item in self.application_list:
                file_2.write(item)
        for child in self.frame.winfo_children():
            if child.winfo_class() == 'Labelframe' :
                child.grid_remove()
        self.label_add_app.configure(text='L\'application a été rajoutée')
        self.root.after(2000, self.root.destroy)
        self.top_button.after(2000, self.top_button.destroy)

#-----------------------------------------------------------------------------

    def erase(self, event):
        """Retire une application"""

        self.app_to_suppress = self.suppress_app.get()
        if self.app_to_suppress.strip().lower() == 'ajouter' or self.app_to_suppress.strip().lower() == 'retirer':
            tkmess.showwarning('Opération non valide', 'Cette opération n\'est pas valide', parent=self.root)
            self.suppress_app.delete(0, 30)
            self.frame.grid_remove()
        else :
            with open(self.dir_name + '/' + self.app_command[1], 'r') as file_ :
                self.application_list = file_.readlines()
                for i, commando in enumerate(self.application_list):
                    if commando.strip().lower()==self.app_to_suppress.strip().lower() and (i+1) % 3 == 0:
                        self.application_list[i-2:i+1] = []
            with open(self.dir_name + '/' + self.app_command[1], 'w') as file_2 :
                for commando in self.application_list:
                    file_2.write(commando)
        for child in self.frame.winfo_children():
            if child.winfo_class() == 'Button' or child.winfo_class() == 'Entry' :
                child.grid_remove()
        self.label_suppress_app.configure(text='L\'application a été retirée')
        self.root.after(2000, self.root.destroy)
        self.top_button.after(2000, self.top_button.destroy)

# == Main programm ===========================================================

if __name__ == "__main__":

    menu = Menu() # Création de l'objet "menu"
    menu.menu() # Appel de la méthode menu() sur l'objet "menu"

